package ejm.admin.model;

import assignment2.cars.model.Car;

import java.time.LocalDateTime;

/**
 * @author Ken Finnigan
 */
public class TestCarObject extends Car {
    public TestCarObject(Integer id) {
        this.id = id;
    }

    TestCarObject(Integer id,
                  String brand,
                  String type,
                  String licencePlate) {
        this.id = id;
        this.brand = brand;
        this.type = type;
        this.licencePlate = licencePlate;
    }
}
