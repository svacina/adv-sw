package ejm.admin;

import java.util.List;
import java.util.Map;

import assignment2.cars.model.Car;
import ejm.admin.model.TestCarObject;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.wildfly.swarm.arquillian.DefaultDeployment;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */
@RunWith(Arquillian.class)
@DefaultDeployment
@RunAsClient
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CarResourceTest {

    @BeforeClass
    public static void setup() throws Exception {
        RestAssured.baseURI = "http://localhost:8080";
    }



    @Test
    public void aRetrieveAllCars() throws Exception {
        Response response =
                when()
                        .get("/car/all")
                .then()
                        .extract().response();

        String jsonAsString = response.asString();
        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");

        assertThat(jsonAsList.size()).isEqualTo(2);

        Map<String, ?> record1 = jsonAsList.get(0);

        assertThat(record1.get("id")).isEqualTo(100);
        assertThat(record1.get("brand")).isEqualTo("Volvo");
        assertThat(record1.get("type")).isEqualTo("XC90");
        assertThat(record1.get("licencePlate")).isEqualTo("ZL 96-98");

        Map<String, ?> record2 = jsonAsList.get(101);

        assertThat(record1.get("id")).isEqualTo(2);
        assertThat(record2.get("brand")).isEqualTo("Volvo");
        assertThat(record2.get("type")).isEqualTo("XC60");
        assertThat(record2.get("licencePlate")).isEqualTo("ZL 95-97");
    }
//
//    @Test
//    public void bRetrieveCar() throws Exception {
//        Response response =
//                given()
//                        .pathParam("carId", 100)
//                .when()
//                        .get("/car/{carId}")
//                .then()
//                        .extract().response();
//
//        String jsonAsString = response.asString();
//
//        Car car = JsonPath.from(jsonAsString).getObject("", Car.class);
//
//        assertThat(car.getId()).isEqualTo(100);
//        assertThat(car.getBrand()).isEqualTo("Volvo");
//        assertThat(car.getType()).isEqualTo("XC90");
//        assertThat(car.getLicencePlate()).isEqualTo("ZL 96-98");
//    }
//
//    @Test
//    public void cCreateCar() throws Exception {
//        Car skoda = new Car();
//        skoda.setId(102);
//        skoda.setBrand("Skoda");
//        skoda.setType("120L");
//        skoda.setLicencePlate("UH 12-47");
//
//        Response response =
//                given()
//                        .contentType(ContentType.JSON)
//                        .body(skoda)
//                .when()
//                        .post("/car");
//
//        assertThat(response).isNotNull();
//        assertThat(response.getStatusCode()).isEqualTo(201);
//        String locationUrl = response.getHeader("Location");
//        Integer carId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 102));
//
//        response =
//                when()
//                        .get("/car/all")
//                .then()
//                        .extract().response();
//
//        String jsonAsString = response.asString();
//        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");
//
//        assertThat(jsonAsList.size()).isEqualTo(3);
//
//        response =
//                given()
//                        .pathParam("carId", carId)
//                .when()
//                        .get("/car/{carId}")
//                .then()
//                        .extract().response();
//
//        jsonAsString = response.asString();
//
//        Car car = JsonPath.from(jsonAsString).getObject("", Car.class);
//
//        assertThat(car.getId()).isEqualTo(carId);
//        assertThat(car.getBrand()).isEqualTo("Volvo");
//        assertThat(car.getType()).isEqualTo("XC90");
//        assertThat(car.getLicencePlate()).isEqualTo("UH 12-47");
//    }

//    @Test
//    public void dFailToCreatecarFromNullName() throws Exception {
//        car badcar = new car();
//        badcar.setVisible(Boolean.TRUE);
//        badcar.setHeader("header");
//        badcar.setImagePath("n/a");
//        badcar.setParent(new TestCarObject(1009));
//
//        Response response =
//                given()
//                        .contentType(ContentType.JSON)
//                        .body(badcar)
//                .when()
//                        .post("/admin/car");
//
//        assertThat(response).isNotNull();
//        assertThat(response.getStatusCode()).isEqualTo(400);
//        assertThat(response.getBody().asString()).contains("Validation failed for classes [ejm.admin.model.car] during persist time for groups [javax.validation.groups.Default, ]\n" +
//                                                                   "List of constraint violations:[\n" +
//                                                                   "\tConstraintViolationImpl{interpolatedMessage='must not be null', propertyPath=name, rootBeanClass=class ejm.admin.model.car, messageTemplate='{javax.validation.constraints.NotNull.message}'}\n" +
//                                                                   "]");
//
//        response =
//                when()
//                        .get("/admin/car")
//                .then()
//                        .extract().response();
//
//        String jsonAsString = response.asString();
//        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");
//
//        assertThat(jsonAsList.size()).isEqualTo(22);
//    }
//
//    @Test
//    public void eUpdateTheCar() throws Exception {
//
//        Response response =
//                given()
//                        .pathParam("carId", 1009)
//                        .when()
//                        .get("/admin/car/{carId}")
//                        .then()
//                        .extract().response();
//
//        String jsonAsString = response.asString();
//
//        car car = JsonPath.from(jsonAsString).getObject("", car.class);
//
//        assertThat(response).isNotNull();
//        assertThat(response.getStatusCode()).isEqualTo(200);
//
//
//
//        assertThat(car.getId()).isEqualTo(1009);
//        assertThat(car.getParent().getId()).isEqualTo(1002);
//        assertThat(car.getName()).isEqualTo("Cars");
//        assertThat(car.isVisible()).isEqualTo(Boolean.TRUE);
//
//        //Modify
//        car.setName("Skoda");
//        car.setVisible(Boolean.FALSE);
//
//
//        response =
//                given()
//                        .contentType(ContentType.JSON)
//                        .pathParam("carId", car.getId())
//                        .body(car)
//                        .when()
//                        .put("/admin/car/{carId}");
//
//        assertThat(response).isNotNull();
//        assertThat(response.getStatusCode()).isEqualTo(200);
//        String locationUrl = response.getHeader("Location");
//        //Integer carId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 1));
//
//        response =
//                when()
//                        .get("/admin/car")
//                        .then()
//                        .extract().response();
//
//        jsonAsString = response.asString();
//        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");
//
//        assertThat(jsonAsList.size()).isEqualTo(22);
//
//        response =
//                given()
//                        .pathParam("carId", 1009)
//                        .when()
//                        .get("/admin/car/{carId}")
//                        .then()
//                        .extract().response();
//
//        jsonAsString = response.asString();
//
//        car = JsonPath.from(jsonAsString).getObject("", car.class);
//
//        assertThat(car.getId()).isEqualTo(1009);
//        assertThat(car.getParent().getId()).isEqualTo(1002);
//        assertThat(car.getName()).isEqualTo("Skoda");
//        assertThat(car.isVisible()).isEqualTo(Boolean.FALSE);
//    }
//
//
//    @Test
//    public void fDeleteCar() throws Exception {
//        // GET
//        Response response =
//                given()
//                        .pathParam("carId", 1019)
//                        .when()
//                        .get("/admin/car/{carId}")
//                        .then()
//                        .extract().response();
//
//        String jsonAsString = response.asString();
//
//        car car = JsonPath.from(jsonAsString).getObject("", car.class);
//
//        assertThat(response).isNotNull();
//        assertThat(response.getStatusCode()).isEqualTo(200);
//
//        assertThat(car.getId()).isEqualTo(1019);
//        assertThat(car.getParent().getId()).isEqualTo(1009);
//        assertThat(car.getName()).isEqualTo("Porsche");
//        assertThat(car.isVisible()).isEqualTo(Boolean.TRUE);
//
//        //DELETE
//        response =
//                given()
//                        .contentType(ContentType.JSON)
//                        .pathParam("carId", car.getId())
//                        .when()
//                        .delete("/admin/car/{carId}");
//
//        assertThat(response).isNotNull();
//        assertThat(response.getStatusCode()).isEqualTo(204);
//
//
//        //GET
//        Response responseGetAfterDelete =
//                given()
//                        .pathParam("carId", 1019)
//                        .when()
//                        .get("/admin/car/{carId}")
//                        .then()
//                        .extract().response();
//
//
//
//        assertThat(responseGetAfterDelete).isNotNull();
//        assertThat(responseGetAfterDelete.getStatusCode()).isEqualTo(204);
//
//
//
//    }



}
