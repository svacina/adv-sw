INSERT INTO person (id, name, email, age) VALUES (1, 'gen. Josef Balaban', 'balaban@acr.cz', 98 );
INSERT INTO person (id, name, email, age) VALUES (2, 'gen. Emil Strankmüller', 'strankmuller@acr.cz', 92);
-- INSERT INTO person (id, name, email, age) VALUES (3, 'gen. Václav Morávek', 'moravek@acr.cz', 96);
-- INSERT INTO person (id, name, email, age) VALUES (4, 'gen. Miloslav Masopust', 'masopust@acr.cz', 89);
-- INSERT INTO person (id, name, email, age) VALUES (5, 'gen. Josef Mašín', 'masin@acr.cz', 95);

--
INSERT INTO car (id, brand, type, licence_plate) VALUES (100, 'Volvo', 'XC90', 'ZL 96-98');
INSERT INTO car (id, brand, type, licence_plate) VALUES (101, 'Volvo', 'XC60', 'ZL 95-97');