package assignment2.cars.rest;

import assignment2.cars.model.Car;
import assignment2.cars.model.Car;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Collection;

@Path("/car")
@ApplicationScoped
public class CarResource {

    @PersistenceContext(unitName = "Cars", type = PersistenceContextType.EXTENDED)
    private EntityManager em;

    @GET
    @Path("/handshake")
    @Produces("text/plain")
    public Response handshake() {
        return Response.ok("Car handshake.").build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{carId}")
    public Car get(@PathParam("carId") Integer carId) {
        return em.find(Car.class, carId);
    }

    @GET
    @Path("/all/{personId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Car> allCarsByPerson(@PathParam("personId") Integer personId) throws Exception {
        return em.createNamedQuery("Car.personCars", Car.class)
                .setParameter("p", personId)
                .getResultList();
    }

    @GET
    @Path("/all/{carId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Car> all() throws Exception {
        return em.createNamedQuery("Car.findAll", Car.class)
                .getResultList();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Response create(Car category) throws Exception {
//        if (category.getId() != null) {
//            return Response
//                    .status(Response.Status.CONFLICT)
//                    .entity("Unable to create Category, id was already set.")
//                    .build();
//        }

        try {
            em.persist(category);
            em.flush();
        } catch (ConstraintViolationException cve) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(cve.getMessage())
                    .build();
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }
        return Response
                .created(new URI("Car/" + category.getId().toString()))
                .build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{CarId}")
    @Transactional
    public Response remove(@PathParam("CarId") Integer CarId) throws Exception {
        try {
            Car entity = em.find(Car.class, CarId);
            em.remove(entity);
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }

        return Response
                .noContent()
                .build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{CarId}")
    @Transactional
    public Response update(@PathParam("CarId") Integer CarId, Car Car) throws Exception {
        try {
            Car entity = em.find(Car.class, CarId);

            if (null == entity) {
                return Response
                        .status(Response.Status.NOT_FOUND)
                        .entity("Category with id of " + CarId + " does not exist.")
                        .build();
            }

            em.merge(Car);

            return Response
                    .ok(Car)
                    .build();
        } catch (Exception e) {
            return Response
                    .serverError()
                    .entity(e.getMessage())
                    .build();
        }
    }


}
