package assignment2.cars.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

@Entity
@Table(name = "car")
@NamedQueries({
        @NamedQuery(name = "Car.findAll", query = "SELECT c from Car c"),
        @NamedQuery(name = "Car.personCars", query = "SELECT c from Car c WHERE c.owner = :personId")
})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Car {


    @Id
    @SequenceGenerator(
            name = "car_sequence",
            allocationSize = 1,
            initialValue = 1020
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_sequence")
    @Column(name = "id", updatable = false, nullable = false)
    protected Integer id;


    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "brand", length = 50, nullable = false)
    protected String brand;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "type", length = 50, nullable = false)
    protected String type;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "licence_plate", length = 50, nullable = false)
    protected String licencePlate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "person_id")
    protected Person owner;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return Objects.equals(getId(), car.getId()) &&
                Objects.equals(getBrand(), car.getBrand()) &&
                Objects.equals(getType(), car.getType()) &&
                Objects.equals(getLicencePlate(), car.getLicencePlate()) &&
                Objects.equals(getOwner(), car.getOwner());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBrand(), getType(), getLicencePlate(), getOwner());
    }
}
