package ejm.adminclient;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.client.utils.URIBuilder;

/**
 * @author Ken Finnigan
 */
public class AdminClient {
    private String url;

    public AdminClient(String url) {
        this.url = url;
    }

    public Category getCategory(final Integer categoryId) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/" + categoryId);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        String jsonResponse =
                Request
                    .Get(uriBuilder.toString())
                    .execute()
                        .returnContent().asString();

        if (jsonResponse.isEmpty()) {
            return null;
        }

        return new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .readValue(jsonResponse, Category.class);
    }

    public Collection<Category> getAllCategories() throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/list");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

//        Content c =
//                Request
//                        .Get(uriBuilder.toString())
//                        .execute()
//                        .returnContent();
//        String jsonResponse = c.toString();
        Response c =
                Request
                        .Get(uriBuilder.toString())
                        .execute();
        Content content = c.returnContent();
        String jsonResponse = content.toString();
        if (jsonResponse.isEmpty()) {
            return null;
        }

        Collection<Category> categories = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .readValue(jsonResponse, new TypeReference<Collection<Category>>() {});
        return categories;
    }

    public Response addCategory(Category categoryToBeAdded) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        Response r =
                (Response) Request
                        .Post(uriBuilder.toString())
                        .bodyByteArray(categoryToBeAdded.toString().getBytes())
                        .execute()
                        .returnResponse();

        if (r == null) {
            return null;
        }

        return r;
    }

    public Response updateCategory(Category categoryToBeAdded) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/" + categoryToBeAdded.getId());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        Response r =
                (Response) Request
                        .Put(uriBuilder.toString())
                        .bodyByteArray(categoryToBeAdded.toString().getBytes())
                        .execute()
                        .returnResponse();

        if (r == null) {
            return null;
        }

        return r;
    }


    public Response removeCategory(final int categoryId) throws IOException {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(url).setPath("/admin/category/" + categoryId);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        Response r =
                (Response) Request
                        .Post(uriBuilder.toString())
                        .execute()
                        .returnResponse();

        if (r == null) {
            return null;
        }

        return r;
    }

}
